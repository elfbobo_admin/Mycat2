# Mycat2

Distributed database based on MySQL or JDBC.

![](https://github.com/MyCATApache/Mycat2/workflows/Java%20CI%20-%20Mycat2%20Main/badge.svg)
![](https://github.com/MyCATApache/Mycat2/workflows/Java%20CI%20-%20Mycat2%20Dev/badge.svg)


[v1.21-release](https://github.com/MyCATApache/Mycat2/releases/tag/v1.21-1-17)

[doc-en](https://www.yuque.com/ccazhw/ml3nkf/bef923fb8acc57e0f805d45ef7782670?translate=en)
[doc-cn](https://www.yuque.com/books/share/6606b3b6-3365-4187-94c4-e51116894695)


[![Stargazers over time](https://starchart.cc/MyCATApache/Mycat2.svg)](https://starchart.cc/MyCATApache/Mycat2)
      




## Framework

![](https://cdn.nlark.com/yuque/0/2021/png/658548/1615792485342-b0f62690-e0cf-4f4a-89b6-18e5e1487227.png)

Currently, only mycat2 is supported on java8, and will be supported in other versions later.



## License

GNU GENERAL PUBLIC LICENSE
